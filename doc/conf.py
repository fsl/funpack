# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

import os.path as op
import datetime
import funpack
import funpack.config as config
import funpack.custom as custom


date      = datetime.date.today()
author    = 'Paul McCarthy'
project   = 'FUNPACK'
release   = '{}'.format(funpack.__version__)
copyright = f'{date.year}, Paul McCarthy, University of Oxford, Oxford, UK'


# substitutions used in various places throughout the docs
custom.registerBuiltIns()
docdir     = op.dirname(op.abspath(__file__))
cfgdir     = op.relpath(funpack.findConfigDir(), docdir)

# See funpack_sphinx_exts.SubstitutionCode -
# hack to allow multi-line substitutions
clihelp    = config.makeParser().format_help().replace('\n', '//n//')
rst_prolog = f"""
.. |cfgdir|           replace:: {cfgdir}
.. |funpack_cli_help| replace:: {clihelp}
"""


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.autosummary',
    'nbsphinx',
    'funpack_sphinx_exts',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------


# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_css_files = ['custom.css']
