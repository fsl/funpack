#!/usr/bin/env bash

set -e

apt-get update -y
apt-get install -y bsdmainutils pandoc

pip install ".[demo,test]"

export PYTHONPATH=$(pwd)/doc
python -m funpack.scripts.generate_notebooks
sphinx-build doc public
