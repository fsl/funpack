#!/bin/bash

set -e

apt-get update -y
apt-get install -y bsdmainutils libhdf5-dev

# install latest FMRIB config from git -
# repository/branch URL can be overridden
# by setting this CI variable
if [ -z "${FUNPACK_FMRIB_CONFIG_URL}" ]; then
  FUNPACK_FMRIB_CONFIG_URL=https://git.fmrib.ox.ac.uk/fsl/funpack-fmrib-config.git
fi

pip install git+${FUNPACK_FMRIB_CONFIG_URL}

pip install --upgrade pip setuptools
pip install --only-binary tables tables
pip install pandas${PANDAS_VERSION}
pip install ".[demo,test]"
pip freeze
pytest
