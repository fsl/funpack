#!/bin/bash

pip install --upgrade pip setuptools wheel
pip install ".[test,demo]"
flake8                           funpack --exclude=funpack/tests || true
pylint --output-format=colorized funpack --ignore=funpack/tests  || true
