#!/usr/bin/env python
#
# cleaning.py - Cleaning functions.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""This module provides the :func:`cleanData` function, which performs
a set of cleaning steps on the data.
"""


import itertools as it
import functools as ft
import              logging

import numpy  as np
import pandas as pd

import funpack.util    as util
import funpack.parsing as parsing


log = logging.getLogger(__name__)


def _evaluateCleaningExpressions(values, col, data):
    """Evaluate any expressions in cleaning values.

    Used to prepare the ``NAValues``, ``RawLevels``, ``NewLevels`` and
    ``ChildValues`` cleaning input parameters.

    For non-numeric/datetime columns, the values are returned unchanged.
    Otherwise, the values are converted into a suitable type, and any value
    expressions are evaluated and replaced with their result (see the
    :mod:`.value_expression` module for details on parsing value expressions)

    :arg values: Sequence of values to prepare.
    :arg col:    A :class:`.Column` describing the column that the values will
                 be used with.
    :arg dtable: The :class:`.DataTable`
    :return:     A ``numpy`` array containing the values.
    """

    if util.isna(values):
        return np.nan

    values = list(values)

    for i, v in enumerate(values):
        if isinstance(v, parsing.ValueExpression):
            values[i] = v(data)

    return np.array(values)


def cleanData(dtable,
              skipNAInsertion=False,
              skipCleanFuncs=False,
              skipChildValues=False,
              skipRecoding=False):
    """Perform data cleaning steps.

    This function does the following:

    1. Re-encodes missing values (the ``NAValues`` column in the variable
       table)
    2. Applies cleaning (the ``Clean`` column in the processing table)
    3. Fills missing values in child variables (the ``ParentValues``
       and ``ChildValues`` columns in the variable table)
    4. Re-encodes categorical variable values (the ``RawLevels`` and
       ``NewLevels`` columns in the variable table)

    :arg dtable:            The :class:`.DataTable`.
    :arg skipNAInsertion:   If ``True``, NA value recoding is skipped.
    :arg skipCleanFuncs:    If ``True``, cleaning functions defined
                            in the variable table are not applied.
    :arg skipChildValues:   If ``True``, child value filling is skipped.
    :arg skipRecoding:      If ``True``, raw-to-new level recoding is skipped.
    :returns:               The :class:`.DataTable`, with cleaned data.
    """

    if not skipNAInsertion:
        with util.timed('NA insertion', log):
            applyNAInsertion(dtable)

    if not skipCleanFuncs:
        with util.timed('Cleaning functions', log):
            applyCleaningFunctions(dtable)

    if not skipChildValues:
        with util.timed('Child value replacement', log):
            applyChildValues(dtable)

    if not skipRecoding:
        with util.timed('Recoding replacement', log):
            applyNewLevels(dtable)

    return dtable


def applyNAInsertion(dtable):
    """Re-codes data which should be interpreted as missing/not available.

    Certain variables can take values which should be interpreted as missing -
    these are defined in the ``NAValues`` columns of the variable and data
    coding tables.

    This function replaces all of those values with ``np.nan``.  The
    replacement is performed in-place.
    """

    # get all variables where
    # NAValues are defined
    vtable = dtable.vartable
    vids   = vtable.index[vtable['NAValues'].notna()]

    log.debug('Recoding missing values as NA for %u variables ...',
              len(vids))

    for vid in vids:

        if not dtable.present(vid):
            continue

        cols   = dtable.columns(vid)
        values = vtable['NAValues'][vid]

        for col in cols:
            data    = dtable[:, col.name]
            cvalues = _evaluateCleaningExpressions(values, col, data)
            navals  = {v : np.nan for v in cvalues}
            dtable[:, col.name] = data.replace(navals)


def _runCleaningFunctions(dtable, procs, vid):
    """Runs cleaning processes for one variable. """

    log.debug('Applying cleaning function to variable %u: %s',
              vid, ','.join([str(p) for p in procs.values()]))

    for proc in procs.values():
        proc.run(dtable, vid)
    return dtable


def applyCleaningFunctions(dtable):
    """Applies cleaning steps specified in the ``Clean`` column of
    the variable table.
    """

    vtable = dtable.vartable
    vids   = vtable.index[vtable['Clean'].notna()]

    allprocs  = []
    allvids   = []
    subtables = []

    for vid in vids:

        if not dtable.present(vid):
            continue

        procs = vtable.loc[vid, 'Clean']
        cols  = dtable.columns(vid)

        allprocs .append(procs)
        allvids  .append(vid)
        subtables.append(dtable.subtable(cols))

    with dtable.pool() as pool:
        subtables = pool.starmap(_runCleaningFunctions,
                                 zip(subtables, allprocs, allvids))

    log.debug('%u cleaning tasks complete - merging results '
              'into main data table.', len(subtables))
    dtable.merge(subtables)


def _runChildValues(dtable, exprs, cvals, vid):
    """Applies child value replacement for the given variable. """

    if not dtable.present(vid):
        return

    # NOTE I'm currently evaluating expressions
    #      *within visit* and *within instance*, i.e.
    #      for a variable at a given visit/instance,
    #      i am evaluating the expression on the
    #      parent variables at the same visit/instance.
    #
    #      Of course this means that I am assuming
    #      the same number of visits/instances are
    #      present for dependent and parent variables.
    #      Replacement on child variables for which
    #      this assumption does not hold is skipped.
    expr      = exprs[           vid]
    cvals     = cvals[           vid]
    visits    = dtable.visits(   vid)
    instances = dtable.instances(vid)

    for visit, instance in it.product(visits, instances):

        # get the column names for this
        # variable, and for each parent
        # variable that correspond to
        # this variable+visit+instance
        col   = dtable.columns(vid, visit, instance)[0]
        cval  = _evaluateCleaningExpressions(cvals, col, dtable[:, col.name])
        pvars = list(set(it.chain(*[e.variables for e in expr])))

        # get the true column names
        # for all parent variables
        # used in the expression
        try:
            pcols = [dtable.columns(v, visit, instance) for v in pvars]

            # if a parent variable is missing,
            # (KeyError or len(pc) == 0) the
            # expression cannot be evaluated.
            # And there should only be one
            # variable for a given
            # (vid, visit, instance)
            if any(len(pc) != 1 for pc in pcols):
                continue

        except KeyError:
            continue

        # Turn this into a dictionary of
        # { vid : column } mappings to
        # pass into the expressions.
        pcols = {pv : pc[0].name for pv, pc in zip(pvars, pcols)}

        log.debug('Evaluating dependency expression for %s...', col.name)

        # Evaluate each expression independently.
        masks = [e.evaluate(dtable[:], pcols) for e in expr]

        # The *last* expression that evalutes to
        # True is the one which defines the
        # replacement child value to use. Here
        # we build a set of indices into the
        # child values, so we use the correct
        # child value for each row. We're
        # limiting to max 255 expressions here.
        idxs = np.zeros(len(dtable), dtype=np.uint8)
        for i, mask in enumerate(masks):
            idxs[mask] = i

        # Now we can combine all expressions
        # to get the final result, and restrict
        # the mask to only affect missing values.
        mask = ft.reduce(lambda a, b: a | b, masks)
        mask = mask & dtable[:, col.name].isna()

        # Finally we apply it to the data.
        dtype = dtable[:, col.name].dtype
        dtable[mask, col.name] = cval[idxs[mask]].astype(dtype)


def applyChildValues(dtable):
    """Fills missing values in variables which have `ParentValues` expressions
    defined.
    """

    # get all variables which have
    # a ParentValues expression
    vtable = dtable.vartable
    mask   = vtable['ParentValues'].notna()
    exprs  = vtable['ParentValues'][mask]
    cvals  = vtable['ChildValues'][ mask]
    vids   = vtable.index[          mask]

    # calculate the order in which the
    # expressions need to be evaluated
    evalOrder = parsing.calculateVariableExpressionEvaluationOrder(vids, exprs)

    log.debug('Recoding missing values on %u dependent variables '
              'from parents (%u hierarchy levels) ...',
              len(vids), len(evalOrder))

    # evaluate and apply the expressions
    # one level at a time, starting from
    # child variables (and those with no
    # dependencies), and finishing with
    # parent variables
    evalOrder = [eo[1] for eo in evalOrder]
    for vidlevel in evalOrder:
        for vid in vidlevel:
            _runChildValues(dtable, exprs, cvals, vid)


def applyNewLevels(dtable):
    """Applies recoding of categorical variables as specified by the
    ``RawLevels`` and ``NewLevels`` columns in the variable table.

    For each column, if the new data (after recoding) is negatively correlated
    with the old data (before recoding), an ``'inverted'`` flag is added
    to the column (via :meth:`.DataTable.addFlag`).
    """

    # get all variables where
    # RawLevels and NewLevels
    # are defined
    vtable    = dtable.vartable
    mask      = vtable['RawLevels'].notna() & vtable['NewLevels'].notna()
    vids      = vtable.index[mask]
    rawlevels = vtable.loc[mask, 'RawLevels']
    newlevels = vtable.loc[mask, 'NewLevels']

    log.debug('Recoding categoricals for %u variables ...',
              len(vids))

    for vid in vids:

        if not dtable.present(vid):
            continue

        for col in dtable.columns(vid):

            data = dtable[:, col.name]

            log.debug('Recoding column %s', col.name)

            # Convert raw/new levels into an appropriate
            # data type. If any values are to be replaced
            # with the data min/max, we don't want the
            # raw level values to be considered in the
            # min/max calculation. So we mask them out
            # (passing that mask to _evaluateCleaningValues).
            raw   = _evaluateCleaningExpressions(rawlevels[vid], col, data)
            mdata = data[~data.apply(raw.__contains__)]
            new   = _evaluateCleaningExpressions(newlevels[vid], col, mdata)

            # replace raw values with new values
            valmap  = dict(zip(raw, new))
            newdata = data.replace(valmap)

            # For numeric columns, check to see if
            # the recoding has caused the data to
            # become inversely correlated with its
            # un-re-encoded version, and add an
            # "inverted" flag accordingly. This
            # will end up in the summary file (see
            # --write_summary)
            if np.issubdtype(data.dtype, np.number):
                if data.corr(newdata) < 0:
                    dtable.addFlag(col, 'inverted')

            dtable[:, col.name] = newdata
