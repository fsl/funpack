# `funpack` merge request


Unless the maintainer is being sloppy, this merge request will not be accepted
unless the following criteria are met:


 - [ ] Unit tests pass
 - [ ] Changelog updated
 - [ ] Version number in `funpack/__init__.py` updated according to
      [Semantic Versioning](https://semver.org) conventions
